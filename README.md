# HTTP Simple Proxy

Simple HTTP Proxy that forwards to another host

## Usage

```bash
docker run -e TARGET=https://www.example.com/ hojerst/simpleproxy
```
