#!/bin/sh

if [ -z "$TARGET" ] ; then
    echo "target url (\$TARGET) must be defined - exiting"
    exit 1
fi

cat >/etc/nginx/conf.d/default.conf <<EOF
server {
  listen      80 default_server;

  access_log off;

  location    / {
    proxy_pass $TARGET;
    proxy_http_version 1.1;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host \$http_host;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Forwarded-For \$remote_addr;
    proxy_set_header X-Forwarded-Port \$server_port;
    proxy_set_header X-Request-Start \$msec;
  }
}
EOF

exec "$@"
